# The Osiris project has moved! It is now available at [https://github.com/nvaytet/osyris](https://github.com/nvaytet/osyris) #

This repository will still be available for legacy purposes but it will no longer be updated.

![demo.png](https://bitbucket.org/repo/jq5boX/images/2936418214-demo.png)
